import Display from "./Display";

const { Component } = require("react");

class Parent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            childDisplay: false
        }
    }

    onBtnClickHandler = () => {
        this.setState({
            childDisplay: true
        })
    }

    onBtnReturn = () => {
        this.setState({
            childDisplay: false
        })
    }

    render() {
        return (
            <div className="text-center mt-5 container bg-dark" style={{ padding: "50px", width: "300px" }}>
                <div>
                    {this.state.childDisplay ? <button className="btn btn-danger" onClick={this.onBtnReturn}>Destroy Component</button> : <button className="btn btn-info text-white" onClick={this.onBtnClickHandler}>Create Component</button>}
                </div>
                <div>
                    {this.state.childDisplay ? <Display /> : null}
                </div>

            </div>

        )
    }
}

export default Parent;