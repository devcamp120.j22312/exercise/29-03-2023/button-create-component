import { Component } from "react";

class Display extends Component {
    constructor(props) {
        super(props);
        this.state = {
            trangThai: false
        }
    }

    componentWillMount = () => {
        console.log("Component Will Mount");
    }

    componentDidMount = () => {
        console.log("Component Did Mount");
    }

    componentWillUnmount = () => {
        console.log("Component Will Unmount");
    }
    render() {
        return(
            <>
                 <h1 style={{color:"green"}}>I exist !</h1>
            </>
        )
    }
}

export default Display;